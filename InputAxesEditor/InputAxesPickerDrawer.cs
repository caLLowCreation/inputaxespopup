﻿#region Author
/*
     
     Jones St. Lewis Cropper (caLLow)
     
     Another caLLowCreation
     
     Visit us on Google+ and other social media outlets @caLLowCreation
     
     Thanks for using our product.
     
     Send questions/comments/concerns/requests to 
      e-mail: caLLowCreation@gmail.com
      subject: InputAxes
     
    Source: 
    https://bitbucket.org/caLLowCreation/inputaxespopup
    git@bitbucket.org:caLLowCreation/inputaxespopup.git
    
    MIT License (MIT)
*/
#endregion

using InputAxes;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace InputAxesEditor
{
    /// <summary>
    /// Draws popup to pick an axes from the InputManager
    /// </summary>
    [CustomPropertyDrawer(typeof(InputAxesPickerAttribute))]
    [CanEditMultipleObjects]
    public sealed class InputAxesPickerDrawer : PropertyDrawer
    {
        const string ASSET_PATH = "ProjectSettings/InputManager.asset";
        const string HELP_FORMAT = "({0}) {1} in script {2} must a string.";
        const int NONE_SELECTED = -1;  //Nothing selected in the popup

        string[] m_AxesNames = null;
        int m_CurrentIndex = NONE_SELECTED;
        InputAxesPickerAttribute m_Attribute = null;
        bool? m_IsValid = null;

        //Cache field validity
        bool isValid
        {
            get { return m_IsValid ?? (m_IsValid = fieldInfo.FieldType.Equals(typeof(string))).Value; }
        }

        //Cache attribute
        InputAxesPickerAttribute attr
        {
            get { return m_Attribute ?? (m_Attribute = attribute as InputAxesPickerAttribute); }
        }

        /// <summary>
        /// Gets height to draw property
        /// </summary>
        /// <param name="property">Property field to draw</param>
        /// <param name="label">Label text and tooltip for property</param>
        /// <returns>Height to draw property</returns>
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (!isValid) return base.GetPropertyHeight(property, label) + EditorGUIUtility.singleLineHeight + 2;

            return base.GetPropertyHeight(property, label);
        }

        /// <summary>
        /// Draws property to the inspector
        /// </summary>
        /// <param name="position">Where to draw the property</param>
        /// <param name="property">Property field to draw</param>
        /// <param name="label">Label text and tooltip for property</param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!isValid)
            {
                EditorGUI.HelpBox(position,
                    string.Format(HELP_FORMAT, fieldInfo.FieldType.Name, fieldInfo.Name, fieldInfo.DeclaringType.Name),
                    MessageType.Error);
                return;
            }

            if (m_AxesNames == null)
            {
                m_AxesNames = InputAxesPickerDrawer.GetInputAxesNames();
                m_CurrentIndex = m_AxesNames.ToList().FindIndex(x => x.Equals(property.stringValue));
            }

            m_CurrentIndex = EditorGUI.Popup(position, attr.label ?? label.text, m_CurrentIndex, m_AxesNames);

            if (GUI.changed && m_CurrentIndex != NONE_SELECTED)
            {
                property.stringValue = m_AxesNames[m_CurrentIndex];
            }
        }

        /// <summary>
        /// Gets an array of the Input Axis names accessible from the InputManager
        /// </summary>
        /// <returns>An array Axis names</returns>
        public static string[] GetInputAxesNames()
        {
            SerializedProperty axesProperty =
                new SerializedObject(AssetDatabase.LoadAllAssetsAtPath(ASSET_PATH)[0])
                .FindProperty("m_Axes");

            var axesNames = new string[axesProperty.arraySize];
            for (int i = 0; i < axesProperty.arraySize; i++)
            {
                axesNames[i] = axesProperty
                    .GetArrayElementAtIndex(i)
                    .FindPropertyRelative("m_Name")
                    .stringValue;
            }

            return axesNames;
        }

    }
}