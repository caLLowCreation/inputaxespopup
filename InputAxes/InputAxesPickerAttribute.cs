﻿#region Author
/*
     
     Jones St. Lewis Cropper (caLLow)
     
     Another caLLowCreation
     
     Visit us on Google+ and other social media outlets @caLLowCreation
     
     Thanks for using our product.
     
     Send questions/comments/concerns/requests to 
      e-mail: caLLowCreation@gmail.com
      subject: InputAxes
     
    Source: 
    https://bitbucket.org/caLLowCreation/inputaxespopup
    git@bitbucket.org:caLLowCreation/inputaxespopup.git

    MIT License (MIT)
*/
#endregion

using UnityEngine;

namespace InputAxes
{
    /// <summary>
    /// Attribute to draw an Input Manager popup to replace the text field slot
    /// </summary>
    public sealed class InputAxesPickerAttribute : PropertyAttribute
    {
        /// <summary>
        /// Label to replace the default field name
        /// </summary>
        public readonly string label;

        /// <summary>
        /// Constructor
        /// </summary>
        public InputAxesPickerAttribute(string label = null)
        {
            this.label = label;
        }
    }
}