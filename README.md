# README #

MIT License (MIT)

These sources are intended for Unity game engine applications.

### What is this repository for? ###

* Attribute to draw a Input Manager popup to replace the text field slot.  Using editor script to draw the popup to pick an axes from the InputManager.
* Version 1.0.0

### How do I get set up? ###

* The source need to be
    - put into Unity Assets directory and Assets/Editor folder
    - compiled into a DLL
        + compile in Visual Studio C# project .NET Framework 3.5.  Create a Plugin folder in the Unity Asset folder and an InputAxes the Assets folder the an Editor folder within the InputAxes: Assets/Plugin/InputAxes/Editor
        + copy InputAxes.dll into the folder InputAxes
        + copy the InputAxesEditor.dll into the InputAxes/Editor folder
* Configuration - None required
* Dependencies - System.dll, UnityEngine.dll, UnityEditor.dll
* Database configuration - None required
* How to run tests - Not explored
* Deployment instructions _ Build with Unity application

### Contribution guidelines ###

* Writing tests - Not explored
* Code review - Not explored
* Other guidelines - Unity assemblies are v5.1.1f1, compile in Visual Studio C# project .NET Framework 3.5

### Who do I talk to? ###

* Repo owner or admin - Jones S. Cropper programmer [caLLowCreation](http://www.callowcreation.com)
* Other community or team contact - Not explored